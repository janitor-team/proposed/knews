/*
 *  Copyright (C) 1995, 1996  Karl-Johan Johnsson.
 */

struct KILL_FILE;
struct KILL_WIDGETS;

extern void	destroy_kill_widgets(struct KILL_WIDGETS*);
extern void	popup_kill_editor(struct KILL_FILE*);
extern void	popdown_kill_editor(struct KILL_WIDGETS*);
extern void	kill_editor_notify_add(struct KILL_FILE*, int);
