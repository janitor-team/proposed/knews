/*
 *  Copyright (C) 1995, 1996  Karl-Johan Johnsson.
 */

enum {
    KillFieldMsgid   = 0,
    KillFieldSubject = 1,
    KillFieldFrom    = 2,
    KillFieldXref    = 3
};

enum {
    KillScopeArticle   = 0,
    KillScopeSubject   = 1,
    KillScopeThread    = 2,
    KillScopeSubthread = 3
};

typedef struct KILL_NODE {
    char		*expr_str;
    regex_t		*expr_re;
    char		*group_str;
    regex_t		*group_re;
    char		*color;
    Pixel		pixel;
    Pixmap		pixmap;
    unsigned int	field         : 2,
			scope         : 2,
			hot           : 1,
			expired       : 1,
			alloced_pixel : 1;
} KILL_NODE;

typedef struct KILL_FILE {
    long		  n;
    struct KILL_NODE	**nodes;
    GROUP		 *group;
    char		 *file_name;
    struct KILL_WIDGETS	 *w;
    unsigned char	  expire;
    unsigned char	  stay_up;
    unsigned char	  dirty;
} KILL_FILE;
