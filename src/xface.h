/*
 *  Copyright (C) 1998  Karl-Johan Johnsson.
 */

extern Pixmap	do_xface(char **headers, int n, long *width, long *height);
