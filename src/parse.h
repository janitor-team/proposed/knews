/*
 *  Copyright (C) 1995, 1996  Karl-Johan Johnsson.
 */

#define PARSEDATE_ERROR ((time_t)(-1))

extern time_t		 parsedate(char *);
extern char		*time_t_to_date(time_t, char*);
extern char		*eat_re(char*);
extern const char	*parse_author(const char*, long*);

extern const char	month_names[];

typedef struct MimeArg {
    char	*name;
    char	*value;
} MimeArg;

typedef enum {
    MimeEncNone, /* 7bit, 8bit and binary */
    MimeEncBase64,
    MimeEncQP,
    MimeEncUue
} MimeEnc;

extern int	 parse_content_enc(char**);
extern int	 parse_content_type(char**, char*, int, char*, int,
				    MimeArg*, int, int);
extern char	*get_charset(MimeArg*);
extern char	*parse_content_disp(char**);

typedef struct {
    char	*word;
    char	*end;
    short	len;
    short	ch_len;
    short	is_qp;
} EncWordData;

extern char	*next_enc_word(char*, EncWordData*);
extern void	 decode_rfc1522(char*, const char*);
