/*
 *  Copyright (C) 1995, 1996  Karl-Johan Johnsson.
 */

struct PostAttachment;

extern struct PostAttachment
		*create_attachment(char*, char*);
extern void	 free_attachment(struct PostAttachment*);
extern int	 print_attachment(FILE*, struct PostAttachment*);
extern void	 print_attach_info(struct PostAttachment*, char*);

extern int	 attach_get_enc(struct PostAttachment*);
extern int	 attach_is_inline(struct PostAttachment*);
extern char	*attach_get_type(struct PostAttachment*);
extern char	*attach_get_name(struct PostAttachment*);
extern char	*attach_get_descr(struct PostAttachment*);

extern int	 attach_set_enc(struct PostAttachment*, int, char*);
extern int	 attach_set_inline(struct PostAttachment*, int, char*);
extern int	 attach_set_type(struct PostAttachment*, char*, char*);
extern int	 attach_set_name(struct PostAttachment*, char*, char*);
extern int	 attach_set_descr(struct PostAttachment*, char*, char*);
