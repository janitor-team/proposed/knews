/*
 *  Copyright (C) 1995, 1996  Karl-Johan Johnsson.
 */

struct PostContext;

extern struct PostContext	*create_post_context(int, char*);
extern void			free_post_context(struct PostContext*);
extern int			outstanding_posts(void);

extern void	post_setup(struct PostContext*, FILE*, ARTICLE*,
			   int, int, int);
extern void	append_signature(FILE*);
extern int	insert_extra_headers(FILE*, ARTICLE*);
