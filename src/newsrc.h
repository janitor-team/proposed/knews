/*
 *  Copyright (C) 1995, 1996  Karl-Johan Johnsson.
 */

extern int	get_newsgroups(void);
extern int	get_newsgroups_from_newsrc(void);
extern int	get_descriptions(void);
extern char    *rescan(void);
extern int	update_newsrc(void);
extern int	check_for_new_groups(void);
extern void	parse_newsrc(int);
extern void	sort_groups(void);
extern GROUP   *create_group(char*);
extern GROUP   *find_group(char*);
