/*
 *  Copyright (C) 1995, 1996  Karl-Johan Johnsson.
 */

struct PostContext;

extern void check_article_to_post(struct PostContext*, Widget);
