/*
 *  Copyright (C) 1995, 1996  Karl-Johan Johnsson.
 */

struct MimeArg;

extern void	partial_build_cache(void);
extern void	partial_clear_cache(void);
extern void	partial_cache_hook(long, struct MimeArg*, int);

