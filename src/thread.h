/*
 *  Copyright (C) 1995, 1996  Karl-Johan Johnsson.
 */

#ifndef THREAD_H
#define THREAD_H

typedef struct THREAD_CONTEXT THREAD_CONTEXT;

extern THREAD_CONTEXT	*main_thr;

extern THREAD_CONTEXT	*create_thread_context(void);
extern void		 clear_thread_context(THREAD_CONTEXT*);
extern ARTICLE		*get_articles(THREAD_CONTEXT*);
extern SUBJECT		*get_subjects(THREAD_CONTEXT*);
extern void		 set_subjects(THREAD_CONTEXT*, SUBJECT*);
extern char		*get_refs(THREAD_CONTEXT*);

extern ARTICLE	*find_article(const char*, long);
extern void	 read_group(char*, int, long);
extern ARTICLE	*parse_head(long, THREAD_CONTEXT*, char*);
extern void	 fix_author(ARTICLE*, char*, int);
extern char	*thread_from_file(struct SERVER*, long);

#endif /* THREAD_H */
