/*
 *  Copyright (C) 1995, 1996  Karl-Johan Johnsson.
 */

struct PostContext;
struct PostWidgets;

extern void	fork_editor(struct PostContext*);
extern void	check_article_and_popup(struct PostContext*);
extern void	destroy_post_widgets(struct PostWidgets*);
