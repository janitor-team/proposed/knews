/*
 *  Copyright (C) 1995, 1996  Karl-Johan Johnsson.
 */

extern struct KILL_NODE	*parse_kill_line(char*, int);
extern void		 fprint_kill_node(FILE*, struct KILL_NODE*, int);
extern void		 sprint_kill_node(struct KILL_NODE*, char*, long);
extern void		 free_kill_node(struct KILL_NODE*);
extern long		 apply_kill(struct KILL_NODE*);
extern void		 fix_node_pixmap(struct KILL_NODE*);
extern void		 alloc_hot_pixel(struct KILL_NODE*, int);
