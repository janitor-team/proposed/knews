/*
 *  Copyright (C) 1995, 1996  Karl-Johan Johnsson.
 */

typedef struct {
    char	*view_command;
    char	*compose;
    char	*compose_typed;
    char	*print;
    char	*edit;
    char	*test;
    char	*x11_bitmap;
    char	*description;
    char	needsterminal;
    char	copiousoutput;
    char	textualnewlines;
} MailcapData;

extern const MailcapData	*mailcap_lookup(char*, char*);
extern void	 mailcap_init(void);
extern char	*expn_tmpl(char*, int, const char*, char**);
