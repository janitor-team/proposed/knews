/*
 *  Copyright (C) 1995, 1996  Karl-Johan Johnsson.
 */

#define SAVE_BOGUS_FROM	(1<<0)
#define SAVE_BOGUS_SUBJ	(1<<1)
#define SAVE_HEAD	(1<<2)
#define SAVE_BODY	(1<<3)
#define SAVE_EMPTY	(1<<4)

extern void	popup_save(void);
extern void	popdown_save(void);
extern void	action_save(Widget, XEvent*, String*, Cardinal*);
extern void	action_pipe(Widget, XEvent*, String*, Cardinal*);
extern void	pipe_context_callback(void*, int, char*);
extern void	set_busy_save(int);
extern int	save_to_file(FILE*, char*, ARTICLE**, long, int, long*);
extern void	text_url_callback(Widget, XtPointer, XtPointer);
