/*
 *  Copyright (C) 1995, 1996  Karl-Johan Johnsson.
 */

extern void	create_post_menu(Widget);

extern void	action_followup(Widget, XEvent*, String*, Cardinal*);
extern void	action_reply(Widget, XEvent*, String*, Cardinal*);
extern void	action_followup_and_reply(Widget, XEvent*, String*, Cardinal*);
extern void	action_post_new(Widget, XEvent*, String*, Cardinal*);
extern void	action_forward_by_mail(Widget, XEvent*, String*, Cardinal*);
