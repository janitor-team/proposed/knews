/*
 *  Copyright (C) 1995, 1996  Karl-Johan Johnsson.
 */

struct KILL_FILE;

extern void		 read_global_kill_file(void);
extern int		 update_kill_files(void);
extern void		 kill_exit_group(GROUP*);
extern void		 kill_cleanup(void);
extern void		 kill_articles(GROUP*);
extern void		 kill_edit_popup(GROUP*);
extern int		 add_kill_node(struct KILL_FILE*, int,
				       int, int, int, char*, char*, char*);
extern struct KILL_FILE	*get_kill_file(GROUP*);
