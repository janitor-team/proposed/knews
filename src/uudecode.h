/*
 *  Copyright (C) 1995, 1996  Karl-Johan Johnsson.
 */

extern void uudecode_callback(Widget, XtPointer, XtPointer);
extern void action_uudecode(Widget, XEvent*, String*, Cardinal*);
