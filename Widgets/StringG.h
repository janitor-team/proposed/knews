/*
 *  Copyright (C) 1995, 1996  Karl-Johan Johnsson.
 */

#ifndef StringG_h
#define StringG_h

#ifndef XtCMargin
#define XtCMargin "Margin"
#endif
#ifndef XtCInternalHeight
#define XtCInternalHeight "InternalHeight"
#endif
#ifndef XtCShadowWidth
#define XtCShadowWidth "ShadowWidth"
#endif
#ifndef XtCCommand
#define XtCCommand "Command"
#endif

#ifndef XtNleftMargin
#define XtNleftMargin "leftMargin"
#endif
#ifndef XtNrightMargin
#define XtNrightMargin "rightMargin"
#endif
#ifndef XtNshadowWidth
#define XtNshadowWidth "shadowWidth"
#endif
#ifndef XtNcommand
#define XtNcommand "command"
#endif

typedef struct StringGadgetClassRec*  StringGadgetClass;
typedef struct StringGadgetRec*       StringGadget;

extern WidgetClass stringGadgetClass;

extern char	*StringGadgetCommand(Widget);

#endif /* StringG_h */
