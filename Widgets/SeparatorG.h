/*
 *  Copyright (C) 1995, 1996  Karl-Johan Johnsson.
 */

#ifndef SeparatorG_h
#define SeparatorG_h

#ifndef XtCSize
#define XtCSize "Size"
#endif
#ifndef XtCShadowWidth
#define XtCShadowWidth "ShadowWidth"
#endif
#ifndef XtCInternalWidth
#define XtCInternalWidth "InternalWidth"
#endif
#ifndef XtCInternalHeight
#define XtCInternalHeight "InternalHeight"
#endif

#ifndef XtNsize
#define XtNsize "size"
#endif
#ifndef XtNShadowWidth
#define XtNShadowWidth "ShadowWidth"
#endif

typedef struct SeparatorGadgetClassRec*  SeparatorGadgetClass;
typedef struct SeparatorGadgetRec*       SeparatorGadget;

extern WidgetClass separatorGadgetClass;

#endif /* SeparatorG_h */
