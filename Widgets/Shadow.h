/*
 *  Copyright (C) 1995, 1996  Karl-Johan Johnsson.
 */

#ifndef Shadow_h
#define Shadow_h

#ifndef XtCAllocArmColor
#define XtCAllocArmColor "AllocArmColor"
#endif
#ifndef XtCAllocArmPixmap
#define XtCAllocArmPixmap "AllocArmPixmap"
#endif
#ifndef XtCAllocShadowColors
#define XtCAllocShadowColors "AllocShadowColors"
#endif
#ifndef XtCShadowWidth
#define XtCShadowWidth "ShadowWidth"
#endif
#ifndef XtCUseLineShadows
#define XtCUseLineShadows "UseLineShadows"
#endif

#ifndef XtNallocArmColor
#define XtNallocArmColor "allocArmColor"
#endif
#ifndef XtNallocArmPixmap
#define XtNallocArmPixmap "allocArmPixmap"
#endif
#ifndef XtNallocShadowColors
#define XtNallocShadowColors "allocShadowColors"
#endif
#ifndef XtNshadowWidth
#define XtNshadowWidth "shadowWidth"
#endif
#ifndef XtNuseLineShadows
#define XtNuseLineShadows "useLineShadows"
#endif

typedef struct ShadowClassRec*  ShadowWidgetClass;
typedef struct ShadowRec*       ShadowWidget;

extern WidgetClass shadowWidgetClass;

extern void ShadowRedrawWidget(Widget);

#endif /* Shadow_h */
