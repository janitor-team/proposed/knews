/*
 *  Copyright (C) 1995, 1996  Karl-Johan Johnsson.
 */

#ifndef Manager_h
#define Manager_h

#ifndef XtCContain
#define XtCContain "Contain"
#endif
#ifndef XtCPreferredHeight
#define XtCPreferredHeight "PreferredHeight"
#endif
#ifndef XtCPreferredWidth
#define XtCPreferredWidth "PreferredWidth"
#endif

#ifndef XtNcontainHoriz
#define XtNcontainHoriz "containHoriz"
#endif
#ifndef XtNcontainVert
#define XtNcontainVert "containVert"
#endif
#ifndef XtNpreferredHeight
#define XtNpreferredHeight "preferredHeight"
#endif
#ifndef XtNpreferredWidth
#define XtNpreferredWidth "preferredWidth"
#endif
#ifndef XtNresizeCallback
#define XtNresizeCallback "resizeCallback"
#endif

typedef struct ManagerRec		*ManagerWidget;
typedef struct ManagerClassRec		*ManagerWidgetClass;

extern WidgetClass managerWidgetClass;

extern void	Remanage(Widget);

#endif /* Manager_h */
