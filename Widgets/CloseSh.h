/*
 *  Copyright (C) 1995, 1996  Karl-Johan Johnsson.
 */

#ifndef CloseSh_h
#define CloseSh_h

#ifndef XtNcursor
#define XtNcursor "cursor"
#endif
#ifndef XtNcloseCallback
#define XtNcloseCallback "closeCallback"
#endif

typedef struct CloseShellClassRec*	CloseShellWidgetClass;
typedef struct CloseShellRec*		CloseShellWidget;

extern WidgetClass closeShellWidgetClass;

extern void	CloseShellSetCursor(Widget, Cursor);

#endif /* CloseSh_h */
