/*
 *  Copyright (C) 1995, 1996  Karl-Johan Johnsson.
 */

#ifndef SeparatoGP_h
#define SeparatoGP_h

#include "SeparatorG.h"
#include "MenuGP.h"

typedef struct {
    XtPointer		extension;
} SeparatorGadgetClassPart;

typedef struct SeparatorGadgetClassRec {
    RectObjClassPart		rect_class;
    MenuGadgetClassPart		menu_g_class;
    SeparatorGadgetClass	separator_g_class;
} SeparatorGadgetClassRec;

extern SeparatorGadgetClassRec separatorGadgetClassRec;

typedef struct {
    Dimension	size;
    Dimension	shadow_width;
    Dimension	internal_width;
    Dimension	internal_height;
    /* private data */
} SeparatorGadgetPart;

typedef struct SeparatorGadgetRec {
    ObjectPart		object;
    RectObjPart		rectangle;
    MenuGadgetPart	menu_g;
    SeparatorGadgetPart	separator_g;
} SeparatorGadgetRec;

#endif /* SeparatoGP_h */
