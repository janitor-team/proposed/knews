/*
 *  Copyright (C) 1995, 1996  Karl-Johan Johnsson.
 */

#ifndef Menu_h
#define Menu_h

#ifndef XtNcursor
#define XtNcursor "cursor"
#endif

typedef struct MenuRec		*MenuWidget;
typedef struct MenuClassRec	*MenuWidgetClass;

extern WidgetClass menuWidgetClass;

extern Widget MenuCreateGadget(String, WidgetClass, Widget, ArgList, Cardinal);

#endif /* Menu_h */
