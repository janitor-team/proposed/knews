/*
 *  Copyright (C) 1995, 1996  Karl-Johan Johnsson.
 */

#ifndef SashP_h
#define SashP_h

#include "Sash.h"
#include "ShadowP.h"

typedef struct {
    int empty;
} SashClassPart;

typedef struct SashClassRec {
    CoreClassPart       core_class;
    ShadowClassPart	shadow_class;
    SashClassPart	sash_class;
} SashClassRec;

extern SashClassRec sashClassRec;

typedef struct {
    XtCallbackList	callback;
    Cursor		cursor;
    /* private data */
    Dimension		pref_width;
    Dimension		pref_height;
} SashPart;

typedef struct SashRec {
    CorePart    core;
    ShadowPart	shadow;
    SashPart	sash;
} SashRec;

#endif /* SashP_h */
