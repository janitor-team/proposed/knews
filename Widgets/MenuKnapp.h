/*
 *  Copyright (C) 1995, 1996  Karl-Johan Johnsson.
 */

#ifndef MenuKnapp_h
#define MenuKnapp_h

#ifndef XtCArrowSize
#define XtCArrowSize "ArrowSize"
#endif
#ifndef XtCArrowOffset
#define XtCArrowOffset "ArrowOffset"
#endif
#ifndef XtCMenuName
#define XtCMenuName "MenuName"
#endif
#ifndef XtCMultiClickTime
#define XtCMultiClickTime "MultiClickTime"
#endif

#ifndef XtNarrowSize
#define XtNarrowSize "arrowSize"
#endif
#ifndef XtNarrowOffset
#define XtNarrowOffset "arrowOffset"
#endif
#ifndef XtNarrowShadowWidth
#define XtNarrowShadowWidth "arrowShadowWidth"
#endif
#ifndef XtNmenuName
#define XtNmenuName "menuName"
#endif
#ifndef XtNmultiClickTime
#define XtNmultiClickTime "multiClickTime"
#endif
#ifndef XtNpopdownTime
#define XtNpopdownTime "popdownTime"
#endif

typedef struct MenuKnappClassRec*	MenuKnappWidgetClass;
typedef struct MenuKnappRec*		MenuKnappWidget;

extern WidgetClass menuKnappWidgetClass;

#endif /* MenuKnapp_h */
