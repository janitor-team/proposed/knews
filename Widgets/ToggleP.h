/*
 *  Copyright (C) 1995, 1996  Karl-Johan Johnsson.
 */

#ifndef ToggleP_h
#define ToggleP_h

#include "Toggle.h"
#include "KnappP.h"

typedef struct {
    XtPointer	empty;
} ToggleClassPart;

typedef struct ToggleClassRec {
    CoreClassPart	core_class;
    ShadowClassPart	shadow_class;
    KnappClassPart	knapp_class;
    ToggleClassPart	toggle_class;
} ToggleClassRec;

extern ToggleClassRec toggleClassRec;

typedef struct {
    Dimension	toggle_size;
    Dimension	toggle_offset;
    Dimension	toggle_shadow_width;
    /* private data */
    Boolean	set;
} TogglePart;

typedef struct ToggleRec {
    CorePart		core;
    ShadowPart		shadow;
    KnappPart		knapp;
    TogglePart		toggle;
} ToggleRec;

#endif /* ToggleP_h */
