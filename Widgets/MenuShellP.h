/*
 *  Copyright (C) 1995, 1996  Karl-Johan Johnsson.
 */

#ifndef MenuShellP_h
#define MenuShellP_h

#include "MenuShell.h"
#include <X11/ShellP.h>

typedef struct {
    XtPointer	extension;
} MenuShellClassPart;

typedef struct MenuShellClassRec {
    CoreClassPart		core_class;
    CompositeClassPart		composite_class;
    ShellClassPart		shell_class;
    OverrideShellClassPart	override_shell_class;
    MenuShellClassPart		menu_shell_class;
} MenuShellClassRec;

extern MenuShellClassRec	menuShellClassRec;

typedef struct {
    XtPointer	extension;
} MenuShellPart;

typedef struct MenuShellRec {
    CorePart		core;
    CompositePart	composite;
    ShellPart		shell;
    OverrideShellPart	override;
    MenuShellPart	menu_shell;
} MenuShellRec;

#endif /* MenuShellP_h */
