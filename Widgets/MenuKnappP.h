/*
 *  Copyright (C) 1995, 1996  Karl-Johan Johnsson.
 */

#ifndef MenuKnappP_h
#define MenuKnappP_h

#include "MenuKnapp.h"
#include "KnappP.h"

typedef struct {
    XtPointer	empty;
} MenuKnappClassPart;

typedef struct MenuKnappClassRec {
    CoreClassPart	core_class;
    ShadowClassPart	shadow_class;
    KnappClassPart	knapp_class;
    MenuKnappClassPart	menu_knapp_class;
} MenuKnappClassRec;

extern MenuKnappClassRec menuKnappClassRec;

enum {
    MenuStateDown,
    MenuStateUp,
    MenuStateWaiting
};

typedef struct {
    String		menu_name;
    int			multi_click_time;
    int			popdown_time;
    Dimension		arrow_size;
    Dimension		arrow_offset;
    Dimension		arrow_shadow_width;
    /* private data */
    Widget		menu;
    Time		start_time;
    int			menu_state;
    XtIntervalId	timer;
} MenuKnappPart;

typedef struct MenuKnappRec {
    CorePart		core;
    ShadowPart		shadow;
    KnappPart		knapp;
    MenuKnappPart	menu_knapp;
} MenuKnappRec;

#endif /* MenuKnappP_h */
