/*
 *  Copyright (C) 1995, 1996  Karl-Johan Johnsson.
 */

#ifndef MenuP_h
#define MenuP_h

#include "Menu.h"
#include "ShadowP.h"
#include "MenuGP.h"

typedef struct {
    XtPointer	extension;
} MenuClassPart;

typedef struct MenuClassRec {
    CoreClassPart	core_class;
    ShadowClassPart	shadow_class;
    MenuClassPart	menu_class;
} MenuClassRec;

extern MenuClassRec menuClassRec;

typedef struct MenuPart {
    Cursor		cursor;
    /* private */
    MenuGadget		*children;
    MenuGadget		current;
    Cardinal		num_children;
    Cardinal		num_slots;
    Dimension		pref_width;
    Dimension		pref_height;
    Boolean		active;
} MenuPart;

typedef struct MenuRec {
    CorePart		core;
    ShadowPart		shadow;
    MenuPart		menu;
} MenuRec;

#endif /* MenuP_h */
