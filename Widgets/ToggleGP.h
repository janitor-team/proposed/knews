/*
 *  Copyright (C) 1995, 1996  Karl-Johan Johnsson.
 */

#ifndef ToggleGP_h
#define ToggleGP_h

#include "ToggleG.h"
#include "StringGP.h"

typedef struct {
    XtPointer		extension;
} ToggleGadgetClassPart;

typedef struct ToggleGadgetClassRec {
    RectObjClassPart		rect_class;
    MenuGadgetClassPart		menu_g_class;
    StringGadgetClassPart	string_g_class;
    ToggleGadgetClassPart	toggle_g_part;
} ToggleGadgetClassRec;

extern ToggleGadgetClassRec toggleGadgetClassRec;

typedef struct {
    Dimension	toggle_size;
    Dimension	toggle_offset;
    Dimension	toggle_shadow_width;
    Boolean	set;
    /* private data */
} ToggleGadgetPart;

typedef struct ToggleGadgetRec {
    ObjectPart		object;
    RectObjPart		rectangle;
    MenuGadgetPart	menu_g;
    StringGadgetPart	string_g;
    ToggleGadgetPart	toggle_g;
} ToggleGadgetRec;

#endif /* ToggleGP_h */
